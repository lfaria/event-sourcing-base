type StreamId = string;
type Message = {
  type: string;
  id: string;
  data: any;
  globalPosition: number;
};
type Evenstore = Record<StreamId, Message[]>;
const eventStore: Evenstore = {};
function listenToUpdates(
  categoryName: string,
  handleFn: (message: Message) => void,
): void {
  setInterval(() => {
    const categoryEvents = Object.keys(eventStore).filter((key) =>
      key.startsWith(categoryName + '-'),
    );
    const mergedEvents: Message[] = [];
    categoryEvents.forEach((key) =>
      eventStore[key].forEach((message) => handleFn(message)),
    );
  }, 1000);
}

eventStore['videoPublishing:commands-XYZ'] = [
  {
    type: 'PublishVideo',
    id: '1',
    globalPosition: 1,
    data: {
      videoId: 'XYZ',
      publishedAt: new Date(),
    },
  },
];
eventStore['videoPublishing-XYZ'] = [
  {
    type: 'VideoPublished',
    id: '1',
    globalPosition: 1,
    data: {
      videoId: 'XYZ',
      publishedAt: new Date(),
    },
  },
];

function getStreamEvents(stream: string): Message[] {
  return eventStore[stream] ?? [];
}

// NameVideoApplication
function nameVideo(videoId: string, name: string) {
  const streamId = 'videoPublishing:commands-' + videoId;
  const events = getStreamEvents(streamId);
  events.push({
    type: 'NameVideo',
    id: '1',
    globalPosition: events.length + 1,
    data: {
      videoId,
      name,
    },
  });
  eventStore[streamId] = events;
}

// VideoComponent
function listen(): void {
  listenToUpdates('videoPublishing:commands', (message) => {
    const videoEvents = eventStore['videoPublishing-' + message.data.videoId];
    const { videoId, name } = message.data;
    const globalPosition = videoEvents.length + 1;
    if (message.globalPosition <= videoEvents.length) {
      return;
    }
    if (!name) {
      videoEvents.push({
        type: 'VideoNameRejected',
        id: '2',
        globalPosition,
        data: {
          videoId,
          reason: 'Invalid name',
        },
      });
    } else {
      videoEvents.push({
        type: 'VideoNamed',
        id: '2',
        globalPosition,
        data: {
          videoId,
          name,
        },
      });
    }
  });
}

describe('PQ', () => {
  it('Should work', () => {
    listen();
    nameVideo('XYZ', 'Vídeo Legal');
    nameVideo('XYZ', '');
    nameVideo('XYZ', '');
    nameVideo('XYZ', 'Vídeo Outro');
  });
});
